from django.db import models
from department.models import DepartmentStudyGroup
from subject.models import Subject
from teacher.models import Teacher
from classroom.models import Classroom

class LessonSheduleTemplate(models.Model):

    DAY_OF_WEEK = [
        ('1', 'Понедельник'),
        ('2', 'Вторник'),
        ('3', 'Среда'),
        ('4', 'Четверг'),
        ('5', 'Пятница'),
        ('6', 'Суббота'),
    ]

    NUMBER_LESSON = [
        ('1', '1 пара'),
        ('2', '2 пара'),
        ('3', '3 пара'),
        ('4', '4 пара'),
        ('5', '5 пара'),
        ('6', '6 пара'),
        ('7', '7 пара'),
        ('8', '8 пара'),
    ]

    TYPE_WEEK = [
        ('0', 'Обычная'),
        ('1', 'Четная'),
        ('2', 'Не четная'),
    ]

    dayweek = models.CharField(max_length=20, choices=DAY_OF_WEEK, default='1')
    studygroup = models.ForeignKey(DepartmentStudyGroup, on_delete=models.CASCADE)
    typeweek =  models.CharField(max_length=20, choices=TYPE_WEEK, default='0')
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    numberlesson = models.CharField(max_length=20, choices=NUMBER_LESSON, default='0')
    teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE)
    classroom = models.ForeignKey(Classroom, on_delete=models.CASCADE)


    class Meta:
        ordering = ["studygroup","dayweek","numberlesson","typeweek"]


class LessonShedule(models.Model):


    NUMBER_LESSON = [
        ('1', '1 пара'),
        ('2', '2 пара'),
        ('3', '3 пара'),
        ('4', '4 пара'),
        ('5', '5 пара'),
        ('6', '6 пара'),
        ('7', '7 пара'),
        ('8', '8 пара'),
    ]

    TYPE_LESSON = [
        ('1', 'Пара'),
        ('2', 'Зачет'),
        ('3', 'Зачет с оценкой'),
        ('3', 'Экзамен'),
    ]

    studygroup = models.ForeignKey(DepartmentStudyGroup, on_delete=models.CASCADE)
    datalesson = models.DateField()
    numberlesson = models.CharField(max_length=20, choices=NUMBER_LESSON, default='1')
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE)
    classroom = models.ForeignKey(Classroom, on_delete=models.CASCADE)
    typelesson = models.CharField(max_length=20, choices=TYPE_LESSON, default='1')


    class Meta:
        ordering = ["id"]


class TypeWeekLesson(models.Model):

    TYPE_WEEK = [
        ('1', 'Четная'),
        ('2', 'Не четная'),
    ]

    databegin = models.DateField()
    dataend = models.DateField()
    typeweek = models.CharField(max_length=20, choices=TYPE_WEEK, default='1')


    class Meta:
        ordering = ["id"]
