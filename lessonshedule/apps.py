from django.apps import AppConfig


class LessonsheduleConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'lessonshedule'
