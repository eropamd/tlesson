from django.contrib import admin
from .models import LessonSheduleTemplate, LessonShedule, TypeWeekLesson
from django.urls import path
from django.http import HttpResponse, HttpResponseRedirect
from department.models import DepartmentStudyGroup
from datetime import date, timedelta, datetime


@admin.register(LessonSheduleTemplate)
class LessonSheduleTemplateAdmin(admin.ModelAdmin):
    list_display = ('id','studygroup', 'dayweek','typeweek','subject','numberlesson','teacher','classroom')
    list_filter = ['typeweek']
    search_fields = ['studygroup__name']


@admin.register(LessonShedule)
class LessonSheduleAdmin(admin.ModelAdmin):
    list_display = ('id','datalesson', 'studygroup', 'numberlesson', 'subject', 'teacher', 'classroom','typelesson')
    list_filter = ('numberlesson', 'datalesson')
    search_fields = ['studygroup__name']
    change_list_template = "admin/create_shedule.html"

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('createshedule/', self.set_immortal),
        ]
        return my_urls + urls

    def set_immortal(self, request):
        #DataStart=datetime.strptime(request.POST['datastart'], '%Y-%m-%d')
        #DataEnd = datetime.strptime(request.POST['dataend'], '%Y-%m-%d')
        DepartmentStudyGroups=DepartmentStudyGroup.objects.filter(isactive=True)
        oddWeek=0;
        for itemGroup in DepartmentStudyGroups:
            DataStart = datetime.strptime(request.POST['datastart'], '%Y-%m-%d')
            DataEnd = datetime.strptime(request.POST['dataend'], '%Y-%m-%d')
            print(itemGroup)
            print(DataStart)
            print(DataEnd)
            # Удоляем по этой учебной группе в эти дни
            deleteRecord = LessonShedule.objects.filter(studygroup=itemGroup, datalesson__range=[DataStart, DataEnd])
            deleteRecord.delete()

            #Получаем тип недли ( четная или нет)
            query = TypeWeekLesson.objects.filter(databegin__lte=DataStart, dataend__gte=DataEnd)
            typeWeekDB=query.first()
            if(typeWeekDB):
                oddWeek=typeWeekDB.typeweek

            # Добовляем запись из шаблона
            delta = timedelta(days=1)
            while DataStart <= DataEnd:
                dataTemplates = LessonSheduleTemplate.objects.filter(studygroup=itemGroup,dayweek=DataStart.weekday()+1)
                try:
                    for dataTemplate in dataTemplates:
                        if dataTemplate.typeweek == '0':
                            new_lesson = LessonShedule(
                                studygroup=itemGroup,
                                datalesson=DataStart,
                                numberlesson=dataTemplate.numberlesson,
                                subject=dataTemplate.subject,
                                teacher=dataTemplate.teacher,
                                classroom=dataTemplate.classroom,
                                typelesson=1
                            )
                            new_lesson.save()
                        if dataTemplate.typeweek == oddWeek:
                            new_lesson = LessonShedule(
                                studygroup=itemGroup,
                                datalesson=DataStart,
                                numberlesson=dataTemplate.numberlesson,
                                subject=dataTemplate.subject,
                                teacher=dataTemplate.teacher,
                                classroom=dataTemplate.classroom,
                                typelesson=1
                            )
                            new_lesson.save()
                except Exception as e:
                    print(f"Error creating lesson schedule: {e}")
                DataStart += delta
        self.message_user(request, "Create shedule !")
        return HttpResponseRedirect("../")


@admin.register(TypeWeekLesson)
class TypeWeekLessonAdmin(admin.ModelAdmin):
    list_display = ('id', 'databegin', 'dataend', 'typeweek')