# Generated by Django 4.2 on 2024-03-10 07:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('department', '0002_departmentstudygroup'),
        ('lessonshedule', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='lessonsheduletemplate',
            name='studygroup',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='department.departmentstudygroup'),
            preserve_default=False,
        ),
    ]
