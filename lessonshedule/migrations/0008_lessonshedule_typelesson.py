# Generated by Django 4.2 on 2024-03-11 08:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lessonshedule', '0007_alter_lessonshedule_options_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='lessonshedule',
            name='typelesson',
            field=models.CharField(choices=[('1', 'Пара'), ('2', 'Зачет'), ('3', 'Зачет с оценкой'), ('3', 'Экзамен')], default='1', max_length=20),
        ),
    ]
