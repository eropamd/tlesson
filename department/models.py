from django.db import models


class Department(models.Model):


    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class DepartmentStudyGroup(models.Model):


    name = models.CharField(max_length=255)
    department = models.ForeignKey(Department, on_delete=models.CASCADE)
    isactive = models.BooleanField(default=True)
    istelegramm = models.BooleanField(default=True)

    def __str__(self):
        return self.name
