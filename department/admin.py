from django.contrib import admin
from .models import Department, DepartmentStudyGroup

admin.site.register(Department)
admin.site.register(DepartmentStudyGroup)