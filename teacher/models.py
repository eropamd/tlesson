from django.db import models


class Teacher(models.Model):


    last_name = models.CharField(max_length=255)
    first_name = models.CharField(max_length=255)
    patronymic =  models.CharField(max_length=255)
    telegramid = models.BigIntegerField(default=0)

    def __str__(self):
        return self.last_name+" / "+self.first_name+" / "+self.patronymic
