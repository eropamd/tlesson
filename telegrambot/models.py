from django.db import models
from department.models import DepartmentStudyGroup

class TelegramUser(models.Model):

    studygroup = models.ForeignKey(DepartmentStudyGroup, on_delete=models.CASCADE)
    telegramid = models.BigIntegerField(default=0)
