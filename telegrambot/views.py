from django.shortcuts import render
from rest_framework.views import APIView  # Import APIView
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
from rest_framework import status
from department.models import Department, DepartmentStudyGroup
from lessonshedule.models import LessonShedule
from datetime import datetime, timedelta
from .models import TelegramUser
from teacher.models import Teacher
from department.models import DepartmentStudyGroup
import http.client
import json
import os

class TelegramBotSend():

    TOKEN=os.environ.get('TELEGRAMM_API')
    URL_WEBHOOK=os.environ.get('TELEGRAMM_URL')

    #test def
    def test(self,chat_id,text_send):
        conn = http.client.HTTPSConnection("api.telegram.org")
        payload = json.dumps({
            "chat_id": chat_id,
            "text": text_send,
            "disable_notification": True
        })
        headers = {
            'Content-Type': 'application/json'
        }
        conn.request("POST", "/bot"+self.TOKEN+"/sendMessage", payload, headers)
        res = conn.getresponse()
        data = res.read()

    def sendTextChat(self,chat_id,text_send):
        conn = http.client.HTTPSConnection("api.telegram.org")
        payload = json.dumps({
            "chat_id": chat_id,
            "text": text_send,
            "disable_notification": True
        })
        headers = {
            'Content-Type': 'application/json'
        }
        conn.request("POST", "/bot"+self.TOKEN+"/sendMessage", payload, headers)
        res = conn.getresponse()
        data = res.read()

    #list department
    def sendListDepartment(self,chat_id):

        data_departments = Department.objects.all()
        arrayData=[];

        for department in data_departments:
            arrayData.append([{"text":department.name, "callback_data": "listdepartment_"+str(department.id)}])

        #arrayData.append([{"text":"расписание(для преподавателя)", "callback_data": "tecahersubject_0"}])
        arrayData.append([{"text": "❤️ Закрепленная группа", "callback_data": "getgroup_0"}])

        first_telegramid = Teacher.objects.filter(telegramid=chat_id).first()
        if first_telegramid is not None:
            arrayData.append([{"text": "🎓 Мое расписание", "callback_data": "getteacher_"+str(first_telegramid.id)}])


        keyboard = { "inline_keyboard": arrayData};
        payload = json.dumps({
            "chat_id": chat_id,
            "reply_markup": keyboard,
            "text":"Выберите кафедру:",
            "disable_notification": True
        })
        headers = {
            'Content-Type': 'application/json'
        }
        conn = http.client.HTTPSConnection("api.telegram.org")
        conn.request("POST", "/bot" + self.TOKEN + "/sendMessage", payload, headers)
        conn.getresponse()

    def listLesson(self,chat_id,value=0):
        today = datetime.now()
        #start_of_week = today - timedelta(days=today.weekday())
        start_of_week = today  + timedelta(hours=3)
        end_of_week = start_of_week + timedelta(days=6)
        dataLessons=LessonShedule.objects.filter(studygroup=value, datalesson__range=[start_of_week, end_of_week])

        ##print(dataLessons)
        arrayData = [];
        arrayData.append([{"text": "Закрепить группу", "callback_data": "setgroup_"+str(value)}])
        arrayData.append([{"text": "на главную", "callback_data": "gohome_0"}])

        textShow=""
        for day in range((end_of_week - start_of_week).days + 1):
            current_day = start_of_week + timedelta(days=day)
            dayShow=current_day.strftime('%Y-%m-%d')
            textShow+=" \n ------ "+current_day.strftime('%Y-%m-%d')+" ------"
            for i in range(1,9,1):
                for dataLesson in dataLessons:
                    if str(dayShow)== str(dataLesson.datalesson):
                        if int(dataLesson.numberlesson)==i:
                            dataLessons.filter(datalesson=dayShow)
                            textShow += " \n" +str(i)+") "+dataLesson.subject.name+" \n"+dataLesson.teacher.last_name+" "+\
                                dataLesson.teacher.first_name+" "+dataLesson.teacher.patronymic+" \n"+\
                                dataLesson.classroom.name


        keyboard = {"inline_keyboard": arrayData};
        payload = json.dumps({
            "chat_id": chat_id,
            "reply_markup": keyboard,
            "text": "Расписание на этой недели:"+textShow,
            "disable_notification": True
        })
        headers = {
            'Content-Type': 'application/json'
        }
        conn = http.client.HTTPSConnection("api.telegram.org")
        conn.request("POST", "/bot" + self.TOKEN + "/sendMessage", payload, headers)
        conn.getresponse()


    def listLessonTeacher(self,chat_id,value=0):
        today = datetime.now()
        #start_of_week = today - timedelta(days=today.weekday())
        start_of_week = today  + timedelta(hours=3)
        end_of_week = start_of_week + timedelta(days=6)
        dataLessons=LessonShedule.objects.filter(teacher_id=value, datalesson__range=[start_of_week, end_of_week])

        ##print(dataLessons)
        arrayData = [];
        arrayData.append([{"text": "на главную", "callback_data": "gohome_0"}])
        first_telegramid = Teacher.objects.filter(id=value).first()
        textShow=""
        for day in range((end_of_week - start_of_week).days + 1):
            current_day = start_of_week + timedelta(days=day)
            dayShow=current_day.strftime('%Y-%m-%d')
            textShow+=" \n ------ "+current_day.strftime('%Y-%m-%d')+" ------"
            for i in range(1,9,1):
                for dataLesson in dataLessons:
                    if str(dayShow)== str(dataLesson.datalesson):
                        if int(dataLesson.numberlesson)==i:
                            dataLessons.filter(datalesson=dayShow)
                            get_groupID = DepartmentStudyGroup.objects.filter(id=value).first()
                            textShow += " \n" +str(i)+") "+dataLesson.subject.name+" \n"+" "+\
                                "Группа :"+get_groupID.name+" \n"+\
                                dataLesson.classroom.name

        keyboard = {"inline_keyboard": arrayData};
        payload = json.dumps({
            "chat_id": chat_id,
            "reply_markup": keyboard,
            "text": "Расписание на этой недели . Преподователь "+first_telegramid.last_name+" "+first_telegramid.first_name+" "+first_telegramid.patronymic+" :"+textShow,
            "disable_notification": True
        })
        headers = {
            'Content-Type': 'application/json'
        }
        conn = http.client.HTTPSConnection("api.telegram.org")
        conn.request("POST", "/bot" + self.TOKEN + "/sendMessage", payload, headers)
        conn.getresponse()


    def getGroupUser(self,chat_id):

        str_text_return = ""
        try:
            groupGet = TelegramUser.objects.get(telegramid=chat_id)
            str_text_return = "У вас есть закрпленная группа "+groupGet.studygroup.name

            today = datetime.now()
            start_of_week = today + timedelta(hours=3)
            end_of_week = start_of_week + timedelta(days=6)
            dataLessons = LessonShedule.objects.filter(studygroup=groupGet.studygroup.id, datalesson__range=[start_of_week, end_of_week])

            textShow = ""
            for day in range((end_of_week - start_of_week).days + 1):
                current_day = start_of_week + timedelta(days=day)
                dayShow = current_day.strftime('%Y-%m-%d')
                textShow += " \n ------ " + current_day.strftime('%Y-%m-%d') + " ------"
                for i in range(1, 9, 1):
                    for dataLesson in dataLessons:
                        if str(dayShow) == str(dataLesson.datalesson):
                            if int(dataLesson.numberlesson) == i:
                                dataLessons.filter(datalesson=dayShow)
                                textShow += " \n" + str(
                                    i) + ") " + dataLesson.subject.name + " \n" + dataLesson.teacher.last_name + " " + \
                                            dataLesson.teacher.first_name + " " + dataLesson.teacher.patronymic + " \n" + \
                                            dataLesson.classroom.name
            str_text_return+=textShow
        except TelegramUser.DoesNotExist:
            str_text_return = "Нету закрепленной группы"


        arrayData = [];
        arrayData.append([{"text": "на главную", "callback_data": "gohome_0"}])
        keyboard = {"inline_keyboard": arrayData};
        payload = json.dumps({
            "chat_id": chat_id,
            "reply_markup": keyboard,
            "text":str_text_return,
            "disable_notification": True
        })
        headers = {
            'Content-Type': 'application/json'
        }
        conn = http.client.HTTPSConnection("api.telegram.org")
        conn.request("POST", "/bot" + self.TOKEN + "/sendMessage", payload, headers)
        conn.getresponse()



    def setGroupUser(self,chat_id,value=0):

        study_group = DepartmentStudyGroup.objects.get(id=value)
        try:
            user = TelegramUser.objects.get(telegramid=chat_id)
            user.studygroup = study_group
            user.save()
        except TelegramUser.DoesNotExist:
            user = TelegramUser.objects.create(telegramid=chat_id, studygroup=study_group)


        arrayData = [];
        arrayData.append([{"text": "на главную", "callback_data": "gohome_0"}])
        keyboard = {"inline_keyboard": arrayData};
        payload = json.dumps({
            "chat_id": chat_id,
            "reply_markup": keyboard,
            "text": "Вы закрепили группу",
            "disable_notification": True
        })
        headers = {
            'Content-Type': 'application/json'
        }
        conn = http.client.HTTPSConnection("api.telegram.org")
        conn.request("POST", "/bot" + self.TOKEN + "/sendMessage", payload, headers)
        conn.getresponse()


    # get list department group
    def sendListDepartmentGroup(self,chat_id,value=0):
        conn = http.client.HTTPSConnection("api.telegram.org")

        data_departments = DepartmentStudyGroup.objects.filter(istelegramm=True)
        arrayData = [];

        for department in data_departments.filter(department=value):
            arrayData.append([{"text": department.name, "callback_data": "groupid_" + str(department.id)}])
        arrayData.append([{"text": "на главную", "callback_data": "gohome_0"}])

        keyboard = {"inline_keyboard": arrayData};
        payload = json.dumps({
            "chat_id": chat_id,
            "reply_markup": keyboard,
            "text": "Выберите группу:",
            "disable_notification": True
        })
        headers = {
            'Content-Type': 'application/json'
        }
        conn.request("POST", "/bot" + self.TOKEN + "/sendMessage", payload, headers)
        conn.getresponse()

    #set url webhook
    def setUrlWebhook(self):
        conn = http.client.HTTPSConnection("api.telegram.org")
        payload = json.dumps({
            "url": "https://telegramlesson.rfpgu.ru/telegramm/",
        })
        headers = {
            'Content-Type': 'application/json'
        }
        conn.request("POST", "/bot" + self.TOKEN + "/setWebhook", payload, headers)
        conn.getresponse()


class TelegrammBotView(APIView):
    renderer_classes = [JSONRenderer]

    def arrayDataGet(self,strParametr):
        my_array = strParametr.split('_')
        return my_array

    def get(self,request):
        bot=TelegramBotSend()
        bot.setUrlWebhook()
        return Response({'message': 'telegram send data'}, status=status.HTTP_200_OK)

    def post(self,request):
        bot = TelegramBotSend()

        #print(request.data)
        if 'callback_query' in request.data:
            telegramSend=request.data['callback_query']['data']
            arrayDataCommand=self.arrayDataGet(telegramSend)
            chatid=request.data['callback_query']['from']['id']
            #print(arrayDataCommand)

            match arrayDataCommand[0]:
                case "listdepartment":
                    bot.sendListDepartmentGroup(chatid,arrayDataCommand[1])

                case "gohome":
                    bot.sendListDepartment(chatid)
                # получить закрепление
                case "getgroup":
                    bot.getGroupUser(chatid)
                # установить закрепление группы
                case "setgroup":
                    bot.setGroupUser(chatid, arrayDataCommand[1])
                #получить расписание группы
                case "groupid":
                    bot.listLesson(chatid,arrayDataCommand[1])
                case "getteacher":
                    bot.listLessonTeacher(chatid,arrayDataCommand[1])
                # если нету комманды
                case _:
                    bot.sendTextChat(chatid,'Нету такой комманды для бота')
        else:
            telegramSend =request.data['message']['text']
            chatid = request.data['message']['from']['id']
            bot.sendTextChat(chatid, 'Нету такой комманды для бота :'+telegramSend)
            bot.sendListDepartment(chatid)
        return Response({'message': 'telega post'}, status=status.HTTP_200_OK)

class TelegrammBotSetUrlView(APIView):
    renderer_classes = [JSONRenderer]

    def get(self, request):
        bot = TelegramBotSend()
        bot.setUrlWebhook()
        return Response({'message': 'telegram set url webhook'}, status=status.HTTP_200_OK)