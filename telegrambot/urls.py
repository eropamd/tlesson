from django.urls import path
from .views import TelegrammBotView, TelegrammBotSetUrlView

urlpatterns = [
    path('telegramm/', TelegrammBotView.as_view(), name='telegram-bot'),
]
